<?php

namespace App\Http\Controllers;

use App\Models\Citizen;
use App\Models\FamilyCitizen;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;

class FamilyCitizenController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request) {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id) {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id) {
        //
    }

    /**
     * Aggiungo il cittadino alla famiglia
     */
    public function addMember(Request $request, string $family_id, string $citizen_id) {
        $validatorArray = array_merge($request->route()->parameters(), $request->all());

        $validator = Validator::make($validatorArray, [
            'family_id' => 'required|exists:families,id',
            'citizen_id' => 'required|exists:citizens,id',
            'role' => 'required|in:Genitore,Tutore,Figlio',
        ]);

        if ($validator->fails()) {
            throw new ConflictHttpException($validator->errors());
        }

        $familyCitizen = FamilyCitizen::where("family_id", $family_id)->where("citizen_id", $citizen_id)->first();

        if ($familyCitizen) {
            throw new ConflictHttpException("Il cittadino è già assegnato alla famiglia");
        } else {
            FamilyCitizen::create([
                "family_id" => $family_id,
                "citizen_id" => $citizen_id,
                "role" => $request->role,
            ])->save();

            return response()->json(["message" => "Cittadino assegnato con successo"]);
        }
    }

    /**
     * Rimuovo il cittadino dalla famiglia
     */
    public function removeMember(Request $request, string $family_id, string $citizen_id) {
        $validatorArray = $request->route()->parameters();

        $validator = Validator::make($validatorArray, [
            'family_id' => 'required|exists:families,id',
            'citizen_id' => 'required|exists:citizens,id',
        ]);

        if ($validator->fails()) {
            throw new ConflictHttpException($validator->errors());
        }

        $familyCitizen = $this->checkMovingCitizen($family_id, $citizen_id);
        FamilyCitizen::destroy($familyCitizen->id);

        return response()->json(["message" => "Cittadino rimosso dalla famiglia con successo"]);
    }

    /**
     * Sposto il cittadino da una famiglia ad un'altra
     */
    public function moveMember(Request $request, string $family_id, string $citizen_id) {
        $validatorArray = array_merge($request->route()->parameters(), $request->all());

        $validator = Validator::make($validatorArray, [
            'family_id' => 'required|exists:families,id',
            'citizen_id' => 'required|exists:citizens,id',
            'new_family_id' => 'required|exists:families,id',
        ]);

        if ($validator->fails()) {
            throw new ConflictHttpException($validator->errors());
        }

        $familyCitizen = $this->checkMovingCitizen($family_id, $citizen_id);

        $newFamilyId = $request->new_family_id;

        $familyCitizen->family_id = $newFamilyId;
        $familyCitizen->save();

        return response()->json(["message" => "Cittadino spostato dalla famiglia " . $family_id . " alla famiglia " . $newFamilyId . " con successo"]);
    }

    /**
     * Promuovo il cittadino a responsabile della famiglia
     */
    public function setResponsible(Request $request, string $family_id, string $citizen_id) {
        $validatorArray = array_merge($request->route()->parameters(), $request->all());

        $validator = Validator::make($validatorArray, [
            'family_id' => 'required|exists:families,id',
            'citizen_id' => 'required|exists:citizens,id',
        ]);

        if ($validator->fails()) {
            throw new ConflictHttpException($validator->errors());
        }

        $familyCitizen = self::checkResponsibleCitizen($family_id, $citizen_id);

        $moreText = "";
        $oldFamilyCitizenResponsible = FamilyCitizen::where("family_id", $family_id)->where("isResponsible", true)->first();

        if ($oldFamilyCitizenResponsible) {
            $citizen = Citizen::where("id", $oldFamilyCitizenResponsible->citizen_id)->first();
            $oldFamilyCitizenResponsible->isResponsible = false;
            $oldFamilyCitizenResponsible->save();
            $moreText = ", rimosso il precedente responsabile " . $citizen->name . " " . $citizen->surname;
        }

        $familyCitizen->isResponsible = true;
        $familyCitizen->save();

        return response()->json(["message" => "Cittadino assegnato come responsabile della famiglia con successo" . $moreText]);
    }

    /*
     * Controllo se il cittadino può uscire dalla sua attuale famiglia (Spostamento o Rimozione)
     */
    public function checkMovingCitizen($family_id, $citizen_id) {
        $familyCitizen = FamilyCitizen::where("family_id", $family_id)->where("citizen_id", $citizen_id)->first();

        if (!$familyCitizen) {
            throw new ConflictHttpException("Il cittadino non è assegnato alla famiglia");
        }

        if ($familyCitizen->isResponsible) {
            throw new ConflictHttpException("Il cittadino responsabile non può lasciare la famiglia");
        }

        $countCitizenFamilies = FamilyCitizen::where("citizen_id", $citizen_id)->count();
        $countFamilyCitizens = FamilyCitizen::where("family_id", $family_id)->count();

        if ($countCitizenFamilies === 1 && $countFamilyCitizens === 1 && $familyCitizen->role === "Figlio") {
            throw new ConflictHttpException("I cittadini figli non possono lasciare la famiglia se sono gli unici membri di quella famiglia e non appartengono già ad altre famiglie");
        }

        return $familyCitizen;
    }

    /*
     * Controllo se il cittadino può diventare responsabile della famiglia
     */
    public function checkResponsibleCitizen($family_id, $citizen_id) {
        $familyCitizen = FamilyCitizen::where("family_id", $family_id)->where("citizen_id", $citizen_id)->first();

        if (!$familyCitizen) {
            throw new ConflictHttpException("Il cittadino non è assegnato a questa famiglia");
        }

        if (!in_array($familyCitizen->role, ["Genitore", "Tutore"])) {
            throw new ConflictHttpException("Il cittadino non è un genitore o un tutore perciò non può essere responsabile di questa famiglia");
        }

        if ($familyCitizen->isResponsible) {
            throw new ConflictHttpException("Il cittadino è già responsabile di questa famiglia");
        }

        if ($familyCitizen->role === "Genitore") {
            $countFamilyMembers = FamilyCitizen::where("family_id", $family_id)->count();

            if ($countFamilyMembers > 6) {
                throw new ConflictHttpException("Il genitore può essere responsabile di famiglie con massimo 6 membri, la famiglia ha " . $countFamilyMembers . " membri");
            }

            $countCitizenResponsibleFamilies = FamilyCitizen::where("citizen_id", $citizen_id)->where("isResponsible", true)->count();

            if ($countCitizenResponsibleFamilies >= 3) {
                throw new ConflictHttpException("Il genitore può essere responsabile per non più di 3 famiglie, il genitore è responsabile di " . $countCitizenResponsibleFamilies . " famiglie");
            }
        }

        return $familyCitizen;
    }
}
