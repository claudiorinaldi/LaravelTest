<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void {
        Schema::create('families_citizens', function (Blueprint $table) {
            $table->id();

            $table->foreignId('family_id')->constrained('families')->onDelete('cascade');
            $table->foreignId('citizen_id')->constrained('citizens')->onDelete('cascade');
            $table->boolean("isResponsible")->default(false);
            $table->enum("role", ["Genitore", "Tutore", "Figlio"]);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void {
        Schema::dropIfExists('families_citizens');
    }
};
