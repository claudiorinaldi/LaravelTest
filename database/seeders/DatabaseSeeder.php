<?php

namespace Database\Seeders;

use App\Models\Citizen;
use App\Models\Family;
use App\Models\User;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void {
        // User::factory(10)->create();

        User::factory()->create([
            'name' => 'Test User',
            'email' => 'test@example.com',
        ]);

        Citizen::factory(10)->create();

        Family::factory(10)->create();

        DB::table('families_citizens')->insert([[
            'citizen_id' => 1,
            'family_id' => 1,
        ], [
            'citizen_id' => 5,
            'family_id' => 2,
        ], [
            'citizen_id' => 3,
            'family_id' => 2,
        ], [
            'citizen_id' => 5,
            'family_id' => 4,
        ], [
            'citizen_id' => 2,
            'family_id' => 1,
        ], [
            'citizen_id' => 1,
            'family_id' => 3,
        ], [
            'citizen_id' => 1,
            'family_id' => 4,
        ]]);
    }
}
