<?php

use App\Http\Controllers\FamilyCitizenController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::prefix('/families')->group(function () {
    Route::put('{family_id}/set-responsible/{citizen_id}', [FamilyCitizenController::class, "setResponsible"])->name("setResponsible");
    Route::put('{family_id}/add-member/{citizen_id}', [FamilyCitizenController::class, "addMember"])->name("addMember");
    Route::put('{family_id}/delete-member/{citizen_id}', [FamilyCitizenController::class, "removeMember"])->name("removeMember");
    Route::put('{family_id}/move-member/{citizen_id}', [FamilyCitizenController::class, "moveMember"])->name("moveMember");
});
