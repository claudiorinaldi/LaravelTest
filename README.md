# Test di Laravel di Claudio Rinaldi

Il progetto permette la gestione dell'anagrafe dei cittadini e le famiglie associate.

## Info

Laravel: 11\
Versione PHP: 8.2.18\
Database: mySql

## Installazione di Base

1. Installa e configura: mySql
2. Copia il file ".env.example", rinominalo ".env" e configura i dati di accesso al database mySql
3. Successivamente lancia i seguenti comandi in ordine
4. Utilizza le chiamate API

## Comandi

`composer update`\
`php artisan key:generate`\
`php artisan migrate:fresh --seed`\
`php artisan serve`

## API

#### Le chiamate sono presenti nel file 'CR_Postman.json' presente nel progetto e sono comunque spiegate qui in seguito: ####

### **Aggiungi cittadino**

Permette di associare un cittadino ad una famiglia\
Metodo: PUT
`localhost:8000/families/{family_id}/add-member/{citizen_id}`

Inserire nel payload: "role" (Scelto tra "Genitore", "Tutore" o "Figlio")

### **Sposta cittadino**

Permette di spostare un cittadino da una famiglia ad un'altra\
Metodo: PUT
`localhost:8000/families/{family_id}/move-member/{citizen_id}`

Inserire nel payload: "new_family_id" (L'id della famiglia di destinazione, deve essere una famiglia esistente)

### **Rimuovi cittadino**

Permette di rimuovere un cittadino da una famiglia\
Metodo: PUT
`localhost:8000/families/{family_id}/delete-member/{citizen_id}`

### **Promuovi cittadino**

Permette di promuovere un cittadino della famiglia a responsabile\
Metodo: PUT
`localhost:8000/families/{family_id}/set-responsible/{citizen_id}`
